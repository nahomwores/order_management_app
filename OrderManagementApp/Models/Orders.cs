﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OrderManagementApp.Models
{
    public class Orders
    {
        [Key] 
        public int Id { get; set; }
        public DateTime OrderDate { get; set; } = DateTime.Now;
        public double TotalAmount { get; set; }
        public string? CustomerName { get { return ApplicationUser.UserName; } }

        //Navigation property order -> user r/n
        public string? ApplicationUserId { get; set; }
        [ForeignKey("ApplicationUserId")]
        public virtual ApplicationUser? ApplicationUser { get; set; }
        

        //OrdersDetail navigation property order -> OrdersDetails many to one
        public virtual List<OrdersDetail> OrdersDetails { get; set; } = new List<OrdersDetail>();
    }

}



