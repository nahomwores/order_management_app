﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OrderManagementApp.Models
{
    public class OrdersDetail
    {
        [Key]
        public int Id { get; set; }
        public int Quantity { get; set; }
        public double UnitPrice { get; set; } 
        public double SubTotal { get; set; }/*{ get { return Quantity * UnitPrice; } }*/
        public double VAT { get; set; } = 0.15;
        public double TaxAmount { get; set; }
        public double Total { get; set; }
        public DateTime OrderDate { get; set; }
       
        //navigation property
        /*public int OrderId { get; set; }
        [ForeignKey("OrderId")]
        public virtual Orders Order { get; set; }*/

        public int ProductId { get; set; }
        [ForeignKey("ProductId")]
        public virtual Product? Product { get; set; }
        public string? ApplicationUserId { get; set; }
        [ForeignKey("ApplicationUserId")]
        public virtual ApplicationUser? ApplicationUser { get; set; }

    }
}
