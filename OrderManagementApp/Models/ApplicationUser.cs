﻿using Microsoft.AspNetCore.Identity;
namespace OrderManagementApp.Models
{
    public class ApplicationUser : IdentityUser
    {

        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? City { get; set; } 
        public string FullName { get { return FirstName + " " + LastName; } } 
        public ICollection<Orders>? Orders { get; set; }
        public string? ProfilePhoto { get; set; }

    }
}
