﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OrderManagementApp.Models
{
    public class Product
    {
        [Key]
        public int Id { get; set; }
        [Display(Name = "Product Name")]
        [Required]
        public string? ProductName { get; set; }
        [Display(Name ="Product Description")]
        [Required]
        public string? Description { get; set; }
        [Display(Name = "Price")]
        [Required]
        public double Price { get; set; }
        public string? Image { get; set; }

        [NotMapped]
        public IFormFile? ImageUpload { get; set; } //ViewModel


    }
}
