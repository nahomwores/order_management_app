﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace OrderManagementApp.Migrations
{
    public partial class final : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_OrdersDetailz_Orderz_OrderId",
                table: "OrdersDetailz");

            migrationBuilder.DropIndex(
                name: "IX_OrdersDetailz_OrderId",
                table: "OrdersDetailz");

            migrationBuilder.DropColumn(
                name: "OrderId",
                table: "OrdersDetailz");

            migrationBuilder.AddColumn<int>(
                name: "OrdersId",
                table: "OrdersDetailz",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_OrdersDetailz_OrdersId",
                table: "OrdersDetailz",
                column: "OrdersId");

            migrationBuilder.AddForeignKey(
                name: "FK_OrdersDetailz_Orderz_OrdersId",
                table: "OrdersDetailz",
                column: "OrdersId",
                principalTable: "Orderz",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_OrdersDetailz_Orderz_OrdersId",
                table: "OrdersDetailz");

            migrationBuilder.DropIndex(
                name: "IX_OrdersDetailz_OrdersId",
                table: "OrdersDetailz");

            migrationBuilder.DropColumn(
                name: "OrdersId",
                table: "OrdersDetailz");

            migrationBuilder.AddColumn<int>(
                name: "OrderId",
                table: "OrdersDetailz",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_OrdersDetailz_OrderId",
                table: "OrdersDetailz",
                column: "OrderId");

            migrationBuilder.AddForeignKey(
                name: "FK_OrdersDetailz_Orderz_OrderId",
                table: "OrdersDetailz",
                column: "OrderId",
                principalTable: "Orderz",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
