﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace OrderManagementApp.Migrations
{
    public partial class OrderDbRevised : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "SubTotal",
                table: "OrdersDetailz",
                type: "float",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "Total",
                table: "OrdersDetailz",
                type: "float",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "VAT",
                table: "OrdersDetailz",
                type: "float",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SubTotal",
                table: "OrdersDetailz");

            migrationBuilder.DropColumn(
                name: "Total",
                table: "OrdersDetailz");

            migrationBuilder.DropColumn(
                name: "VAT",
                table: "OrdersDetailz");
        }
    }
}
