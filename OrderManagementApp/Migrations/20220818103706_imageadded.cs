﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace OrderManagementApp.Migrations
{
    public partial class imageadded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SubTotal",
                table: "OrdersDetailz");

            migrationBuilder.AddColumn<string>(
                name: "Image",
                table: "productz",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Image",
                table: "productz");

            migrationBuilder.AddColumn<double>(
                name: "SubTotal",
                table: "OrdersDetailz",
                type: "float",
                nullable: false,
                defaultValue: 0.0);
        }
    }
}
