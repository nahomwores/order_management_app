﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace OrderManagementApp.Migrations
{
    public partial class OrdersDbEdited : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "TotalAmount",
                table: "Orderz",
                type: "float",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TotalAmount",
                table: "Orderz");
        }
    }
}
