﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace OrderManagementApp.Migrations
{
    public partial class revised : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ApplicationUserId",
                table: "OrdersDetailz",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "OrderDate",
                table: "OrdersDetailz",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.CreateIndex(
                name: "IX_OrdersDetailz_ApplicationUserId",
                table: "OrdersDetailz",
                column: "ApplicationUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_OrdersDetailz_AspNetUsers_ApplicationUserId",
                table: "OrdersDetailz",
                column: "ApplicationUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_OrdersDetailz_AspNetUsers_ApplicationUserId",
                table: "OrdersDetailz");

            migrationBuilder.DropIndex(
                name: "IX_OrdersDetailz_ApplicationUserId",
                table: "OrdersDetailz");

            migrationBuilder.DropColumn(
                name: "ApplicationUserId",
                table: "OrdersDetailz");

            migrationBuilder.DropColumn(
                name: "OrderDate",
                table: "OrdersDetailz");
        }
    }
}
