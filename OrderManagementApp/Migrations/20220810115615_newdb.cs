﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace OrderManagementApp.Migrations
{
    public partial class newdb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_OrdersDetailz_Orderz_OrdersId",
                table: "OrdersDetailz");

            migrationBuilder.DropForeignKey(
                name: "FK_Orderz_AspNetUsers_ApplicationUserId",
                table: "Orderz");

            migrationBuilder.RenameColumn(
                name: "OrdersId",
                table: "OrdersDetailz",
                newName: "OrderId");

            migrationBuilder.RenameIndex(
                name: "IX_OrdersDetailz_OrdersId",
                table: "OrdersDetailz",
                newName: "IX_OrdersDetailz_OrderId");

            migrationBuilder.AlterColumn<string>(
                name: "ApplicationUserId",
                table: "Orderz",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)");

            migrationBuilder.AddForeignKey(
                name: "FK_OrdersDetailz_Orderz_OrderId",
                table: "OrdersDetailz",
                column: "OrderId",
                principalTable: "Orderz",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Orderz_AspNetUsers_ApplicationUserId",
                table: "Orderz",
                column: "ApplicationUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_OrdersDetailz_Orderz_OrderId",
                table: "OrdersDetailz");

            migrationBuilder.DropForeignKey(
                name: "FK_Orderz_AspNetUsers_ApplicationUserId",
                table: "Orderz");

            migrationBuilder.RenameColumn(
                name: "OrderId",
                table: "OrdersDetailz",
                newName: "OrdersId");

            migrationBuilder.RenameIndex(
                name: "IX_OrdersDetailz_OrderId",
                table: "OrdersDetailz",
                newName: "IX_OrdersDetailz_OrdersId");

            migrationBuilder.AlterColumn<string>(
                name: "ApplicationUserId",
                table: "Orderz",
                type: "nvarchar(450)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_OrdersDetailz_Orderz_OrdersId",
                table: "OrdersDetailz",
                column: "OrdersId",
                principalTable: "Orderz",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Orderz_AspNetUsers_ApplicationUserId",
                table: "Orderz",
                column: "ApplicationUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
