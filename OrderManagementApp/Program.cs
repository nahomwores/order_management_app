using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.EntityFrameworkCore;
using OrderManagementApp.Data;
using OrderManagementApp.Data.Service;
using OrderManagementApp.Models;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
//Add session
builder.Services.AddMemoryCache();
builder.Services.AddSession(options =>
{
    //session clears in a 1 day
    options.IdleTimeout = TimeSpan.FromDays(1);
});

builder.Services.AddControllersWithViews();
//Add OrderDbContext 
builder.Services.AddDbContext<OrderDbContext>(options =>
    options.UseSqlServer(builder.Configuration.GetConnectionString("OrdersConnection")));

//Add Interface
builder.Services.AddScoped<IProductService, ProductService>();
//Add Identity
builder.Services.AddIdentity<ApplicationUser, IdentityRole>(options =>
{
    options.Password.RequiredLength = 3;
    options.Password.RequiredUniqueChars = 0;
    options.Password.RequireNonAlphanumeric = false;
}).AddEntityFrameworkStores<OrderDbContext>();

//Add Auth policy
builder.Services.AddMvc(config => {
    var policy = new AuthorizationPolicyBuilder()
                    .RequireAuthenticatedUser()
                    .Build();
    config.Filters.Add(new AuthorizeFilter(policy));
});

//Add Claim Policy
builder.Services.AddAuthorization(options =>
{
    options.AddPolicy("DeleteRole",
        policy => policy.RequireClaim("Delete Role"));
}
        );
var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();
//use session
app.UseSession();
app.UseRouting();
app.UseAuthentication();
//use auth
app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Product}/{action=Index}/{id?}");

app.Run();
