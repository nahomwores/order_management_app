﻿namespace OrderManagementApp.ViewModels
{
    public class UserRolesManagerViewModel
    {
        public string RoleId { get; set; }
        public string RoleName { get; set; }
        public bool IsSelected { get; set; } = false;
    }
}
