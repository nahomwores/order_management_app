﻿using System.ComponentModel.DataAnnotations;

namespace OrderManagementApp.ViewModels
{
    public class EditUserViewModel
    {
        public string? UserId { get; set; }
        [Required]
        public string? UserName { get; set; }
        [EmailAddress]
        [Required]
        public string? Email { get; set; }

        public IList<string>? Roles { get; set; } = new List<string>();
        public List<string>? Claims { get; set; } = new List<string>();
    }
}
