﻿using Microsoft.AspNetCore.Mvc;
using OrderManagementApp.Data;
using System.ComponentModel.DataAnnotations;

namespace OrderManagementApp.ViewModels
{
    //This viewmodel contains the model for the registration form view
    public class RegisterViewModel
    {
        
        [Required]
        [EmailAddress]
        [Remote(action: "IsEmailInUse", controller: "Account")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [Required]
        [DataType(DataType.Password)]
        [Compare("Password",
            ErrorMessage = "Password and confirm password do not match.")]
        public string ConfirmPassword { get; set; }

        //Extended Custom ApplicationUser Class
        [Required]
        public string? FirstName { get; set; }
        [Required]
        public string? LastName { get; set; }

        public IFormFile ImageUpload { get; set; }
        public string City { get; set; }
        public string PhoneNumber { get; set; }
    }
}
