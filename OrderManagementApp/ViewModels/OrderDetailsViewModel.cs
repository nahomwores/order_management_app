﻿using OrderManagementApp.Models;

namespace OrderManagementApp.ViewModels
{
    public class OrderDetailsViewModel
    {
        public OrdersDetail OrdersDetail { get; set; }
        public double GrandTotal { get; set; }
    }
}
