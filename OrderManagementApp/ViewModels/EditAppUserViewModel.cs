﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace OrderManagementApp.ViewModels
{
    public class EditAppUserViewModel
    {
        [Required]
        [EmailAddress]
        //[Remote(action: "IsEmailInUse", controller: "Account")]
        public string Email { get; set; }
        public string? UserName { get; set; }

        [DataType(DataType.Password)]
        public string? Password { get; set; }
        
        [DataType(DataType.Password)]
        [Compare("Password",
            ErrorMessage = "Password and confirm password do not match.")]
        public string? ConfirmPassword { get; set; }

        //Extended Custom ApplicationUser Class
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        public string? ProfilePhoto { get; set; }
        public string? FullName { get; set;}
        public IFormFile? ImageUpload { get; set; }
    }
}
