﻿using OrderManagementApp.Models;

namespace OrderManagementApp.ViewModels
{
    public class CartViewModel
    {
        public List<CartItem> CartItems { get; set; }
        public double GrandTotal { get; set; }
    }
}
