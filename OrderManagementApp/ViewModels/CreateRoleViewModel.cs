﻿using System.ComponentModel.DataAnnotations;

namespace OrderManagementApp.ViewModels
{
    public class CreateRoleViewModel
    {
        [Required]
        public string RoleName { get; set; }
    }
}
