﻿using System.ComponentModel.DataAnnotations;

namespace OrderManagementApp.ViewModels
{
    public class EditRoleViewModel
    {
        public string Id { get; set; }
        [Required]
        public string RoleName { get; set; }
        public List<string> Userz { get; set; } = new List<string>();
    }
}
