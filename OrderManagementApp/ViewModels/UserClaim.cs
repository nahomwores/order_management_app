﻿namespace OrderManagementApp.ViewModels
{
    public class UserClaim
    {
        public string ClaimType { get; set; }
        public bool IsSelected { get; set; } = false;
    }
}
