﻿using OrderManagementApp.Data.Cart;

namespace OrderManagementApp.ViewModels
{
    public class ShoppingCartVM
    {
        public ShoppingCart ShoppingCart { get; set; }
        public double ShoppingCartTotal { get; set; }
    }
}
