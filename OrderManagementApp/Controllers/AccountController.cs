﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using OrderManagementApp.Models;
using OrderManagementApp.ViewModels;
using System.Globalization;

namespace OrderManagementApp.Controllers
{
    public class AccountController : Controller
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly SignInManager<ApplicationUser> signInManager;
        private readonly IPasswordHasher<ApplicationUser> passwordHasher;
        private readonly IWebHostEnvironment webHostEnvironment;

        //Constructor Injection -> inject UserManager and SigninManager
        public AccountController(UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IPasswordHasher<ApplicationUser> passwordHasher,
            IWebHostEnvironment webHostEnvironment)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.passwordHasher = passwordHasher;
            this.webHostEnvironment = webHostEnvironment;
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        //Remote validation allows a controller action method to be called using client side script. 
        //ASP.NET Core MVC uses jQuery remote() method which in turn issues an AJAX call to invoke the server side method. 
        //The jQuery remote() method expects a JSON response, this is the reason we are returning JSON response from the server-side method(IsEmailInUse)
        //Remote Validation to check if the provided email address is in use or not it gives us instant feed back using serverside action
        // use [remote] in the register viewmodel
        [AcceptVerbs("Get", "Post")]
        [AllowAnonymous]
        public async Task<IActionResult> IsEmailInUse(string email)
        {
            var result = await userManager.FindByEmailAsync(email);
            if(result == null)
            {
                return Json(true);
            }
            else
            {
                return Json($"Email {email} is already in use");
            }
        }

        //Registers a user when the submit button is clicked in the view it issues an HttpPost request HttpPost: Account/Register which comes to this action method and excutes the code inside
        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            //Validation 
            if (ModelState.IsValid)
            {
                string photo = await PhotoUploadAsync(model);
                //copy data from RigisterViewModel to IdentityUser
                var user = new ApplicationUser()
                {
                    UserName = model.Email,
                    Email = model.Email,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    ProfilePhoto = photo
                    
                };
                // Store user data in AspNetUsers database table
                var result = await userManager.CreateAsync(user, model.Password);

                // If user is successfully created, sign-in the user using
                // SignInManager and redirect to index action of HomeController
                if (result.Succeeded)
                {
                    if(signInManager.IsSignedIn(User) && User.IsInRole("Admin"))
                    {
                        return RedirectToAction("ListUsers", "Admins");
                    }
                    await signInManager.SignInAsync(user, isPersistent: false);
                    return RedirectToAction("Index", "Product");
                }
                // If there are any errors, add them to the ModelState object
                // which will be displayed by the validation summary tag helper
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
            }
            return View(model);
        }
        
        //Logout functionality using the buit in signoutasync() method
        [HttpPost] 
        public async Task<IActionResult> Logout()
        {
            await signInManager.SignOutAsync();
            return RedirectToAction("Index", "Product");
        }

        [AllowAnonymous]
        //a get request to display the the login view page 
        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [AllowAnonymous]
        //an HttpPost requst to login a user 
        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel model, string? returnUrl)
        {
            //model validation
            if (ModelState.IsValid)
            {
                //sign in the user using .PasswordSignInAsync() method that lives in signInManager class
                var result = await signInManager
                    .PasswordSignInAsync(model.Email, model.Password, model.RememberMe, false);
                if(result.Succeeded)
                {
                    //check if the returnUrl query string has value or not and url.isLocalUrl makes sure the returnUrl is local so that we prevent hackers attack
                    //after successfully signing in redirect me to the page i wanted to go through model binding of query string and action parameter
                    if(!string.IsNullOrEmpty(returnUrl) && Url.IsLocalUrl(returnUrl))
                    {
                        return Redirect(returnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Index", "Product");
                    }
                    
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Invalid Username Or Password");
                }
            }
            return View(model);
        }
        public async Task<IActionResult> AccountDetail()
        {
            ApplicationUser appUser = await userManager.GetUserAsync(User);
            EditAppUserViewModel editUser = new EditAppUserViewModel()
            {
                Email = appUser.Email,
                Password = appUser.PasswordHash,
                FirstName = appUser.FirstName,
                LastName = appUser.LastName,
                FullName = appUser.FullName,
                ProfilePhoto = appUser.ProfilePhoto,
                UserName = appUser.UserName
            };
            return View(editUser);
        }
        //Get/Account/EditAccount
        public async Task<IActionResult> EditAccount()
        {
            ApplicationUser appUser = await userManager.GetUserAsync(User);
            EditAppUserViewModel editUser = new EditAppUserViewModel()
            { 
                Email = appUser.Email,
                Password = appUser.PasswordHash,
                FirstName = appUser.FirstName,
                LastName = appUser.LastName,
                ProfilePhoto = appUser.ProfilePhoto
            };
            return View(editUser);
        }
        //Post/Account/EditAccount
        [HttpPost]
        public async Task<IActionResult> EditAccount(EditAppUserViewModel user)
        {
            string photo = await UploadAsync(user);
            ApplicationUser appUser = await userManager.GetUserAsync(User);
            if (ModelState.IsValid)
            {
                appUser.UserName = user.Email;
                appUser.Email = user.Email;
                appUser.FirstName = user.FirstName;
                appUser.LastName = user.LastName;
                appUser.ProfilePhoto = photo;
                if(user.Password != null)
                {
                    appUser.PasswordHash = passwordHasher.HashPassword(appUser, user.Password);
                }
                IdentityResult result = await userManager.UpdateAsync(appUser);
                if (result.Succeeded)
                {
                    return RedirectToAction("EditAccount"); ;
                }
                else
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                   
                }
            }
            return View();
        }



        //Photo upload
        private async Task<string> PhotoUploadAsync(RegisterViewModel model)
        {
            string fileName = null;
            if (model.ImageUpload != null)
            {
                string folder = Path.Combine(webHostEnvironment.WebRootPath, "ProfilePhotos");
                fileName = Guid.NewGuid().ToString() + "_" + model.ImageUpload.FileName;
                string folder_fileName = Path.Combine(folder, fileName);
                FileStream fs = new FileStream(folder_fileName, FileMode.Create);
                await model.ImageUpload.CopyToAsync(fs);
                fs.Close();
            }
            return fileName;
        } 
        private async Task<string> UploadAsync(EditAppUserViewModel model)
        {
            string fileName = null;
            if (model.ImageUpload != null)
            {
                string folder = Path.Combine(webHostEnvironment.WebRootPath, "ProfilePhotos");
                fileName = Guid.NewGuid().ToString() + "_" + model.ImageUpload.FileName;
                string folder_fileName = Path.Combine(folder, fileName);
                FileStream fs = new FileStream(folder_fileName, FileMode.Create);
                await model.ImageUpload.CopyToAsync(fs);
                fs.Close();
            }
            return fileName;
        }
    }
}
