﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OrderManagementApp.Data;
using OrderManagementApp.Models;
using OrderManagementApp.ViewModels;
using System.Security.Claims;

namespace OrderManagementApp.Controllers
{
    public class OrdersDetailController : Controller
    {
        private readonly OrderDbContext context;
        private readonly UserManager<ApplicationUser> userManager;

        public OrdersDetailController(OrderDbContext context, UserManager<ApplicationUser> userManager)
        {
            this.context = context;
            this.userManager = userManager;
        }


        public async  Task<IActionResult> Index()
        {
            ApplicationUser user = await userManager.GetUserAsync(User);
            List<OrdersDetail> orderDetails = context.OrdersDetailz
                .Include(x => x.ApplicationUser)
                .Include(x => x.Product)
                .Where(x => x.ApplicationUser.Id == user.Id)
                .ToList();
            return View(orderDetails);
            
        }
        public async Task<IActionResult> Add(int id)
        {
            ApplicationUser user = await userManager.GetUserAsync(User);
            //Get the ordered product
            var product = await context.productz.FirstOrDefaultAsync(x => x.Id == id);

            var orderDetails = await context.OrdersDetailz
                .Include(x => x.ApplicationUser)
                .Include(x => x.Product)
                .Where(x => x.ProductId == product.Id)
                .Where(x => x.ApplicationUser.Id == user.Id)
                .FirstOrDefaultAsync();
            
            OrdersDetail od = new OrdersDetail();
            
            if (orderDetails == null)
            {
                od.ProductId = product.Id;
                od.UnitPrice = product.Price;
                od.Quantity = 1;
                od.OrderDate = DateTime.Now;
                od.ApplicationUserId = User.FindFirstValue(ClaimTypes.NameIdentifier);
                od.SubTotal = od.UnitPrice * od.Quantity;
                od.TaxAmount = (od.VAT * od.SubTotal);
                od.Total = od.TaxAmount + od.SubTotal;
                context.OrdersDetailz.Add(od);
                await context.SaveChangesAsync();
            }
            else
            {
                od = orderDetails;
                od.Quantity += 1;
                od.OrderDate = DateTime.Now;
                od.ApplicationUserId = User.FindFirstValue(ClaimTypes.NameIdentifier);
                od.SubTotal = od.UnitPrice * od.Quantity;
                od.TaxAmount = (od.VAT * od.SubTotal);
                od.Total = od.TaxAmount + od.SubTotal;
                context.Update(od);
                context.SaveChanges();
            }

            
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> Decrease(int id)
        {
            ApplicationUser user = await userManager.GetUserAsync(User);

            var od = await context.OrdersDetailz
                .Include(x => x.Product)
                .Include(x => x.ApplicationUser)
                .Where(x => x.Id == id)
                .Where(x => x.ApplicationUser.Id == user.Id)
                .FirstOrDefaultAsync();
           
            if (od.Quantity > 1)
            {
                --od.Quantity;
                od.SubTotal = od.UnitPrice * od.Quantity;
                od.TaxAmount = (od.VAT * od.SubTotal);
                od.Total = od.TaxAmount + od.SubTotal;
                context.OrdersDetailz.Update(od);
               
            }
            else
            {
                context.OrdersDetailz.Remove(od);
                
            }
            context.SaveChanges();

            return RedirectToAction("Index");
        }

        public async Task<IActionResult> RemoveAll()
        {
            ApplicationUser user = await userManager.GetUserAsync(User);
            List<OrdersDetail> ods = await context.OrdersDetailz
                .Include(x => x.ApplicationUser)
                .Include(x => x.Product)
                .Where(x => x.ApplicationUser.Id == user.Id)
                .ToListAsync();
           
            context.OrdersDetailz.RemoveRange(ods);
            context.SaveChanges();
            return RedirectToAction("Index");
        }

        
        public async Task<IActionResult> Orders()
        {
            List<OrdersDetail> orderDetails = await context.OrdersDetailz
                .Include(x => x.Product)
                .Include(x => x.ApplicationUser)
                .ToListAsync();
            return View(orderDetails);
        }
    }
}
