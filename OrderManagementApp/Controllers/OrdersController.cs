﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OrderManagementApp.Data;
using OrderManagementApp.Models;
using System.Security.Claims;

namespace OrderManagementApp.Controllers
{
  
    public class OrdersController : Controller
    {
        private readonly OrderDbContext context;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly SignInManager<ApplicationUser> signInManager;

        public OrdersController(OrderDbContext context, UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager)
        {
            this.context = context;
            this.userManager = userManager;
            this.signInManager = signInManager;
        }

       public async Task<IActionResult> Index()
        {
            ApplicationUser user = await userManager.GetUserAsync(User);

            List<Orders> orders = await context.Orderz
               .Include(x => x.OrdersDetails)
               .Include(x => x.ApplicationUser)
               .ToListAsync();

            Orders order = context.Orderz
                .Include(x => x.OrdersDetails)
                .Include(x => x.ApplicationUser)
                .FirstOrDefault();
            return View(order);
        }

        public async Task<IActionResult> Add()
        {
            ApplicationUser user = await userManager.GetUserAsync(User);

            List<OrdersDetail> ordersDetail = await context.OrdersDetailz
                .Include(x => x.Product)
                .ToListAsync();

            Orders order = new Orders();
           
            order.OrderDate = DateTime.Now;
            order.ApplicationUser = user;
            order.ApplicationUserId = user.Id;


            foreach (var item in ordersDetail)
            {
                //order.OrdersDetails.Add(item);
                order.TotalAmount += item.Total;
            }
            context.Orderz.Add(order);
            await context.SaveChangesAsync();
           
            return RedirectToAction("Index");
        }
        

        /*public async Task<IActionResult> RemoveAll()
        {
            List<OrdersDetail> ods = await context.OrdersDetailz.ToListAsync();

            context.OrdersDetailz.RemoveRange(ods);
            context.SaveChanges();
            return RedirectToAction("Index");
        }*/
    }
   
}
