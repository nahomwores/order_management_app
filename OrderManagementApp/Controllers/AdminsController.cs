﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using OrderManagementApp.Models;
using OrderManagementApp.ViewModels;
using System.Security.Claims;

namespace OrderManagementApp.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminsController : Controller
    {
        private readonly RoleManager<IdentityRole> roleManager;
        private readonly UserManager<ApplicationUser> userManager;

        public AdminsController(RoleManager<IdentityRole> roleManager,
            UserManager<ApplicationUser> userManager)
        {
            this.roleManager = roleManager;
            this.userManager = userManager;
        }


        /*Create Role*/
        //HttpGet: Admins/CreateRole 
        [HttpGet]
        public IActionResult CreateRole()
        {
            return View();
        }
        //when the role form is submitted this post controller takes the
        //data(createRoleViewModel) from the form and assigns the value(RoleName)
        //to IdentityRole table's Name property/column
        [HttpPost]
        public async Task<IActionResult> CreateRole(CreateRoleViewModel model)
        {
            //Check for Model Validation
            if (ModelState.IsValid)
            {
                //assigns the Name property of IdentityRole class with model.RoleName
                IdentityRole identityRole = new IdentityRole()
                {
                    Name = model.RoleName
                };
                // Saves the role in the underlying AspNetRoles table 
                var result = await roleManager.CreateAsync(identityRole);
                if (result.Succeeded)
                {
                    return RedirectToAction("GetAllRoles", "Admins");
                }
                else
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                }
            }
            return View(model);

        }

        [Authorize(Roles ="Admin, Moderator")]
        //Get list of roles
        [HttpGet]
        public IActionResult GetAllRoles()
        {
            //assign all the roles from the roles table to
            //the variable role and pass the it to the view
            var roles = roleManager.Roles.ToList();
            return View(roles);
        }

        /*Edit role*/
        [HttpGet]
        public async Task<IActionResult> EditRole(string id)//pass the id from the url through model binding
        {
            //find the role using the id    
            var role = await roleManager.FindByIdAsync(id);
            if(role == null)
            {
                return NotFound();
            }
            //assign values from the role to the viewmodel EditRoleViewModel properties 
             EditRoleViewModel model = new EditRoleViewModel
            {
                Id = role.Id,
                RoleName = role.Name
            };
            // Loop through all the Users
            foreach (var user in userManager.Users.ToList())
            {
                // If the user is in this role, add the username to
                // Users property of EditRoleViewModel. This model
                // object is then passed to the view for display
                if (await userManager.IsInRoleAsync(user, role.Name))
                {
                    model.Userz.Add(user.UserName);
                }
                else
                {
                    continue;
                }
            }
            return View(model);
        }
        //Edit the Role Name
        //when the form for EditRole is submitted the this post action recives the new name and updates it
        [HttpPost]
        public async Task<IActionResult> EditRole(string id, EditRoleViewModel model) //takes EditRoleViewModel and role id as a parameter
        {
            //gets the role from the roles table using role id
            var role = await roleManager.FindByIdAsync(id);
            if (role == null)
            {
                return NotFound();
            }
            else
            {
                //assigns the new RoleName to Name prop of role class in the db table
                role.Name = model.RoleName;
                //updates the new role name in the table using UpdateAsync
                var result = await roleManager.UpdateAsync(role);
                if (result.Succeeded)
                {
                    return RedirectToAction("GetAllRoles");
                }
                else
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                }
                return View(model);
            }
        }
        [HttpGet]
        public async Task<IActionResult> EditUsersInRole(string roleId)
        {
            //find the role name
            var role = await roleManager.FindByIdAsync(roleId);
            ViewBag.roleId = roleId;
            ViewData["roleName"] = role.Name;
            if(role == null)
            {
                return NotFound();
            }

            //initialize list of UserRoleViweModel
            var listOfUserRoles = new List<UserRoleViewModel>();
            //iterate through all the users in the AspNetUsers table
            foreach (var user in userManager.Users.ToList())
            {
                //instantiate a new userRoleViewModel and copy the userName and id or the user from the users tabel to the userroleviewmodel class properties
                var userRoleViewModel = new UserRoleViewModel
                {
                    UserId = user.Id,
                    UserName = user.UserName,
                };
                //check whether the user from the userTable exists in this role if true make the IsSelected property true else make it false
                if(await userManager.IsInRoleAsync(user, role.Name))
                {
                    userRoleViewModel.IsSelected = true;   
                }
                else
                {
                    userRoleViewModel.IsSelected = false;
                }
                //add the userRoleViewModel props to the list we created
                listOfUserRoles.Add(userRoleViewModel);
            }
            return View(listOfUserRoles);
        }
        [HttpPost]
        public async Task<IActionResult> EditUsersInRole(List<UserRoleViewModel>
            userRolesList, string roleId)
        {
            var role = await roleManager.FindByIdAsync(roleId);
            if(role == null)
            {
                return NotFound();
            }

            IdentityResult result = null;
            for (int i = 0; i < userRolesList.Count; i++)
            {
                var user = await userManager.FindByIdAsync(userRolesList[i].UserId);
                if (userRolesList[i].IsSelected && !(await userManager.IsInRoleAsync(user, role.Name)))
                {
                     result = await userManager.AddToRoleAsync(user, role.Name);
                }
                else if (!(userRolesList[i].IsSelected) && await userManager.IsInRoleAsync(user, role.Name))
                {
                     result = await userManager.RemoveFromRoleAsync(user, role.Name);
                }
                else
                {
                    continue;
                }

                if(result.Succeeded)
                {
                    if (i < (userRolesList.Count - 1))
                        continue;
                    else
                        return RedirectToAction("EditRole", new { id = roleId });
                }    
            }
            return RedirectToAction("EditRole", new { id = roleId });
        }

        [HttpGet]
        public IActionResult ListUsers()
        {
           var user = userManager.Users.ToList();
            return View(user);
        }

        [HttpGet]
        public async Task<IActionResult> EditUser(string id)
        {
            var user = await userManager.FindByIdAsync(id);
            if (user == null)
            {
                return NotFound();
            }
            
                // GetClaimsAsync retunrs the list of user Claims
                var userClaims = await userManager.GetClaimsAsync(user);

                // Gets the list of role names the specified user belongs to
                var userRoles = await userManager.GetRolesAsync(user);

                var model = new EditUserViewModel
                {
                    UserId = user.Id,
                    UserName = user.UserName,
                    Email = user.Email,
                    Roles = userRoles,
                    Claims = userClaims.Select(c => c.Value).ToList(),
                };
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> EditUser(EditUserViewModel model, string id)
        {
            var user = await userManager.FindByIdAsync(id);
            if(user == null)
            {
                return NotFound();
            }

            //overwrite the ff fields with the new values we changed in the EditUser view form
            user.UserName = model.UserName;
            user.Email = model.Email;
            //update the specified user in the user table
            var result = await userManager.UpdateAsync(user);
            if (result.Succeeded)
            {
                return RedirectToAction("ListUsers", "Admins");
            }
            else
            {
                foreach(var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
            }
            return View(model);

        }

        [HttpGet]
        public async Task<IActionResult> UserRolesManager(string userid)
        {
            ViewData["userid"] = userid;
            var user = await userManager.FindByIdAsync(userid);
            if(user == null)
            {
                return NotFound();
            }
            var UserRolesList = new List<UserRolesManagerViewModel>();

            foreach (var role in roleManager.Roles.ToList())
            {
                var userRoles = new UserRolesManagerViewModel
                {
                    RoleId = role.Id,
                    RoleName = role.Name,
                };
                if (await userManager.IsInRoleAsync(user, role.Name))
                {
                    userRoles.IsSelected = true;
                }
                else
                {
                    userRoles.IsSelected = false;
                }
                UserRolesList.Add(userRoles);
            }
            return View(UserRolesList);
        }
        [HttpPost]
        public async Task<IActionResult> UserRolesManager(
            List<UserRolesManagerViewModel> userRoles, string userId)
        {
            var user = await userManager.FindByIdAsync(userId);
            if(user == null)
            {
                return NotFound();
            }
            //Gets a list of role names the specified user belongs to
            var roles = await userManager.GetRolesAsync(user);
            //removes the specified user from the named roles
            var result = await userManager.RemoveFromRolesAsync(user, roles);

            if (!result.Succeeded)
            {
                ModelState.AddModelError("", "Cannot remove user existing roles");
                return View(userRoles);
            }
            //Adds the specified user to the named roles
            /*go to the userRoles viewmodel that was passed and use the Linq prop 
            .Where() to get the selected roles and pass only the rolename using .select()*/
            result = await userManager.AddToRolesAsync(user,
                userRoles.Where(r => r.IsSelected).Select(r => r.RoleName));

            if (!result.Succeeded)
            {
                ModelState.AddModelError("", "Cannot add selected roles to user");
                return View(userRoles);
            }
            return RedirectToAction("EditUser", new {id = userId});

        }

        [HttpPost]
        public async Task<IActionResult> DeleteUser(string id)
        {
            var user = await userManager.FindByIdAsync(id);
            if (user == null)
            {
                return NotFound();
            }
            //deletes the specified user in the backing store
            var result = await userManager.DeleteAsync(user);
            if (result.Succeeded)
            {
                return RedirectToAction("ListUsers");
            }
            else
            {
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
            }
            return View("ListUsers");
        }
        public async Task<IActionResult> DeleteRole(string id)
        {
            var role = await  roleManager.FindByIdAsync(id);
            if(role == null)
            {
                return NotFound();
            }
            else
            {
                var result = await roleManager.DeleteAsync(role);
                if (result.Succeeded)
                {
                    return RedirectToAction("GetAllRoles");
                }
                else
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                }
            }
            return View("GetAllRoles");
        }

        [HttpGet]
        public async Task<IActionResult> ManageUserClaims(string userId)
        {
            var user = await userManager.FindByIdAsync(userId);
            if(user == null)
            {
                return NotFound();
            }

            var existingUseClaims = await userManager.GetClaimsAsync(user);

            var model = new UserClaimsViewModel
            {
                UserId = user.Id
            };

            foreach (var claim in ClaimsStore.AllClaims)
            {
                var userclaim = new UserClaim
                {
                    ClaimType = claim.Type
                };

                if (existingUseClaims.Contains(claim))
                {
                    userclaim.IsSelected = true;
                }
                model.Claims.Add(userclaim);

            }
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> ManageUserClaims(
            UserClaimsViewModel model, string userid)
        {
            var user = await userManager.FindByIdAsync(userid);
            if(user == null)
            {
                return NotFound();
            }

            var userclaims = await userManager.GetClaimsAsync(user);
            var result = await userManager.RemoveClaimsAsync(user, userclaims);
            if (!result.Succeeded)
            {
                ModelState.AddModelError(string.Empty, "Cant delete the claim");
                return View(model);
            }

           result = await userManager.AddClaimsAsync(user,
                model.Claims.Where(c => c.IsSelected)
                .Select(c => new Claim(c.ClaimType, c.ClaimType)));
            if (!result.Succeeded)
            {
                ModelState.AddModelError(string.Empty, "Can not add the follwing claim to the user");
                return View(model);
            }
            return RedirectToAction("EditUser", new {Id = model.UserId});

        }
    }
}
