﻿using Microsoft.AspNetCore.Mvc;
using OrderManagementApp.Data;
using OrderManagementApp.Models;
using OrderManagementApp.ViewModels;

namespace OrderManagementApp.Controllers
{
    public class CartController : Controller
    {
        private readonly OrderDbContext context;

        public CartController(OrderDbContext context)
        {
            this.context = context;
        }
        public IActionResult Index()
        {
            List<CartItem> cart = HttpContext.Session.GetJson<List<CartItem>>("Cart")
                ?? new List<CartItem>();

            CartViewModel cartVM = new CartViewModel
            {
                CartItems = cart,
                GrandTotal = cart.Sum(x => x.Price * x.Quantity)
            };

            return View(cartVM);
        }
        //Get:cart/add/id  
        public async Task<IActionResult> Add(int id)
        {
            //get the product
            var product = await context.productz.FindAsync(id);

            //list of CartItem
            List<CartItem> cart = HttpContext.Session.GetJson<List<CartItem>>("Cart")
                ?? new List<CartItem>();

            //get the product from the cart
            var cartItem = cart.Where(x => x.ProductId == id).FirstOrDefault();
            //if cart is empty add the product else increament the quantity by one
            if(cartItem == null)
            {
                cart.Add(new CartItem(product));
            }
            else
            {
                cartItem.Quantity += 1; 
            }
            HttpContext.Session.SetJson("Cart", cart);
            return RedirectToAction("Index");
        }

        //Get: /cart/decrease/id
        public IActionResult Decrease(int id)
        {


            List<CartItem> cart = HttpContext.Session.GetJson<List<CartItem>>("Cart");
               

            var cartItem = cart.Where(x => x.ProductId == id).FirstOrDefault();
            if (cartItem.Quantity > 1)
            {
                --cartItem.Quantity;
            }
            else
            {
                cart.RemoveAll(x => x.ProductId == id);
            }
            HttpContext.Session.SetJson("Cart", cart);
            if (cart.Count == 0)
            {
                HttpContext.Session.Remove("Cart");
            }
            
            return RedirectToAction("Index");
        }

        //Get: /cart/remove/id
        public IActionResult Remove(int id)
        {


            List<CartItem> cart = HttpContext.Session.GetJson<List<CartItem>>("Cart");
            cart.RemoveAll(x => x.ProductId == id);

            HttpContext.Session.SetJson("Cart", cart);
            if (cart.Count == 0)
            {
                HttpContext.Session.Remove("Cart");
            }
            
            return RedirectToAction("Index");
        }
        //Get: /cart/clear
        public IActionResult Clear()
        {

            HttpContext.Session.Remove("Cart");
            
            return RedirectToAction("Index");
        }
    }
}
