﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OrderManagementApp.Data;
using OrderManagementApp.Data.Service;
using OrderManagementApp.Models;

namespace OrderManagementApp.Controllers
{
    public class ProductController : Controller
    {
        private readonly IProductService service;
        private readonly IWebHostEnvironment webHostEnvironment;
        private readonly OrderDbContext context;

        public ProductController(IProductService service,
            IWebHostEnvironment webHostEnvironment,
            OrderDbContext context)
        {
            this.service = service;
            this.webHostEnvironment = webHostEnvironment;
            this.context = context;
        }
        [AllowAnonymous]
        public async Task<IActionResult> Index(string searchString)
        {
            var product = from p in context.productz
                         select p;
            if(!string.IsNullOrEmpty(searchString))
            {
                product.Where(s => s.ProductName.Contains(searchString));
            }
            return View(await product.ToListAsync());
           /* var products = await service.GetAllAsync();
            return View(products);  */
        }
        public async Task<IActionResult> Details(int id)
        {
            var result = await service.GetByIdAsync(id);
            return View(result);
        }


        [HttpGet]
        public IActionResult Add()
        {
            return View();  
        }

        [HttpPost]
        public async Task<IActionResult> Add(Product product)
        {
            
            string photo = await PhotoUploadAsync(product);
            product.Image = photo;
            await service.Add(product);
            return RedirectToAction("Index", "Product");
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            if (id == 0)
            {
                return View("Error");
            }
            var product = await service.GetByIdAsync(id);
            return View(product);
        }
        [HttpPost]
        public async Task<IActionResult> Edit(int id, Product product)
        {
            string? image = product.Image;
            string path = Path.Combine(webHostEnvironment.WebRootPath, "img");
            string oldpath = Path.Combine(path, "image");
            if (product.ImageUpload != null)
            {
                 
                if (System.IO.File.Exists(oldpath))
                {
                    System.IO.File.Delete(oldpath);
                }
                string photo = await PhotoUploadAsync(product);
                product.Image = photo;
            }
           
            await service.UpdateAsync(id, product);
            return RedirectToAction("Details", new {id = id});
        }

        [HttpGet]
        public async Task<IActionResult> Delete(int id)
        {
            var item  = await service.GetByIdAsync(id);
            if(item == null)
            {
                return View("Error");
            }
            return View(item);
        }

        [HttpPost, ActionName("Delete")]
        public async Task<IActionResult> DeleteProduct(int id)
        {
           
            await service.DeleteAsync(id);
            return RedirectToAction("Index", "Product");
        }

        //Photo upload
        private async Task<string> PhotoUploadAsync(Product product)
        {
            string fileName = null;
            if (product.ImageUpload != null)
            {
                string folder = Path.Combine(webHostEnvironment.WebRootPath, "img");
                fileName = Guid.NewGuid().ToString() + "_" + product.ImageUpload.FileName;
                string folder_fileName = Path.Combine(folder, fileName);
                FileStream fs = new FileStream(folder_fileName, FileMode.Create);
                await product.ImageUpload.CopyToAsync(fs);
                fs.Close();
            }
            return fileName;
        }
    }
}
