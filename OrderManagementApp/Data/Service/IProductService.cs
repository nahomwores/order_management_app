﻿using OrderManagementApp.Models;

namespace OrderManagementApp.Data.Service
{
    public interface IProductService
    {
        Task<IEnumerable<Product>> GetAllAsync();
        Task<Product> GetByIdAsync(int id);
        Task Add(Product product);
        Task<Product> UpdateAsync(int id, Product product);
        Task DeleteAsync(int id);

    }
}
