﻿using Microsoft.EntityFrameworkCore;
using OrderManagementApp.Models;

namespace OrderManagementApp.Data.Service
{
    public class ProductService : IProductService
    {
        private readonly OrderDbContext orderDbContext;

        public ProductService(OrderDbContext orderDbContext)
        {
            this.orderDbContext = orderDbContext;
        }

        public async Task Add(Product product)
        {
            await orderDbContext.productz.AddAsync(product);
            await orderDbContext.SaveChangesAsync();            
        }

        public async Task DeleteAsync(int id)
        {
            var product = await orderDbContext.productz
                .FirstOrDefaultAsync(p => p.Id == id);
            orderDbContext.productz.Remove(product);
            await orderDbContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<Product>> GetAllAsync()
        {
           var products = await orderDbContext.productz.ToListAsync();
            return products;
        }

        public async Task<Product> GetByIdAsync(int id)
        {
            var product = await orderDbContext.productz.
                FirstOrDefaultAsync(p => p.Id == id);
            return product;
        }

        public async Task<Product> UpdateAsync(int id, Product product)
        {
            
            orderDbContext.productz.Update(product);
            await orderDbContext.SaveChangesAsync();
            return product;
        }
    }
}
