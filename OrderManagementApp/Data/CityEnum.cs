﻿namespace OrderManagementApp.Data
{
    public enum CityEnum
    {
        AddisAbaba,
        Adama,
        BahirDar,
        DireDawa,
        Hawassa,
        Mekele
    }
}
